package com.springboot.api.gradle.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.api.gradle.model.Cliente;
import com.springboot.api.gradle.service.impl.ClienteServiceImpl;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
	
	@Autowired
	private ClienteServiceImpl _ClienteService;
	
	@GetMapping(value = "/all", produces = "application/json")	
	public List<Cliente> getAllClientes(){
		return _ClienteService.getAllClientes();
	}
	
	@GetMapping(value = "/get/{id}", produces = "application/json")	
	public Map<String,Object> getCliente(@PathVariable ("id") Integer id){
		Map<String, Object> result = new HashMap<>();
		result.put("codigo_servicio", "0000");
		result.put("producto", _ClienteService.getCliente(id));
		return result;
	}
	
	@PostMapping(value = "/save", produces = "application/json")	
	public List<Cliente> saveCliente(@RequestBody Cliente Cliente){
		
		_ClienteService.saveCliente(Cliente);
		
		return _ClienteService.getAllClientes();
	}	
	
	@DeleteMapping(value = "/delete/{id}", produces = "application/json")	
	public List<Cliente> deleteCliente(@PathVariable ("id") Integer id){
		
		_ClienteService.deleteCliente(id);
		
		return _ClienteService.getAllClientes();
	}	

}
