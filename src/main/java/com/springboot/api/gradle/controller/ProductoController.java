package com.springboot.api.gradle.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.api.gradle.model.Producto;
import com.springboot.api.gradle.service.impl.ProductoServiceImpl;

@RestController
@RequestMapping("/Producto")
public class ProductoController {
	
	@Autowired
	private ProductoServiceImpl _ProductoService;
	
	@GetMapping(value = "/all", produces = "application/json")	
	public List<Producto> getAllProductos(){
		return _ProductoService.getAllProductos();
	}
	
	@GetMapping(value = "/get/{id}", produces = "application/json")	
	public Producto getProducto(@PathVariable ("id") Integer id){
		return _ProductoService.getProducto(id);
	}
	
	@PostMapping(value = "/save", produces = "application/json")	
	public List<Producto> saveProducto(@RequestBody Producto Producto){
		
		_ProductoService.saveProducto(Producto);
		
		return _ProductoService.getAllProductos();
	}	
	
	@DeleteMapping(value = "/delete/{id}", produces = "application/json")	
	public List<Producto> deleteProducto(@PathVariable ("id") Integer id){
		
		_ProductoService.deleteProducto(id);
		
		return _ProductoService.getAllProductos();
	}	

}
