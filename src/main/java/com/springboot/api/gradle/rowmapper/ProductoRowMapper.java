package com.springboot.api.gradle.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.springboot.api.gradle.model.Producto;

public class ProductoRowMapper implements RowMapper<Producto>{

	@Override
	public Producto mapRow(ResultSet rs, int rowNum) throws SQLException {
		Producto Producto = new Producto();
		
		Producto.setId(rs.getInt("id"));
		Producto.setDescripcion(rs.getString("descripcion"));
		Producto.setCategoria(rs.getString("categoria"));
		Producto.setPrecio_unitario(rs.getString("precio_unitario"));
		Producto.setStock_actual(rs.getString("stock_actual"));
		Producto.setStock_minimo(rs.getString("stock_minimo"));
		Producto.setEstado(rs.getString("estado"));
		
		return Producto;
	}

}
