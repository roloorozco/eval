package com.springboot.api.gradle.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.springboot.api.gradle.model.Cliente;

public class ClienteRowMapper implements RowMapper<Cliente>{

	@Override
	public Cliente mapRow(ResultSet rs, int rowNum) throws SQLException {
		Cliente Cliente = new Cliente();
		
		Cliente.setId(rs.getInt("id"));
		Cliente.setNombres(rs.getString("nombres"));
		Cliente.setApellido_pat(rs.getString("apellido_pat"));
		Cliente.setApellido_pat(rs.getString("apellido_mat"));
		Cliente.setSexo(rs.getString("sexo"));
		Cliente.setDireccion(rs.getString("direccion"));
		Cliente.setEstado(rs.getInt("estado"));
		
		return Cliente;
	}

		
}
