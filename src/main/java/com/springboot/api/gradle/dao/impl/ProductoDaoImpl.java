package com.springboot.api.gradle.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.springboot.api.gradle.dao.ProductoDao;
import com.springboot.api.gradle.model.Producto;
import com.springboot.api.gradle.rowmapper.ProductoRowMapper;

@Repository
public class ProductoDaoImpl extends JdbcDaoSupport implements ProductoDao {

	public ProductoDaoImpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}
	
	@Override
	public List<Producto> getAllProductos() {
		logger.debug("::::: Mensaje de prueba :::::::");
		List<Producto> listaProductos = new ArrayList<Producto>();
		
		String sql = " SELECT id, nombres, apellidoPaterno, apellidoMaterno, sexo, direccion, estadoCivil\n" + 
				" FROM api_test.Producto";
		
		try {
			
			RowMapper<Producto> ProductoRow = new ProductoRowMapper();
			listaProductos = getJdbcTemplate().query(sql, ProductoRow);
			logger.debug("Se han listado "+listaProductos.size()+" Productos");
					
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return listaProductos;
	}

	@Override
	public Producto getProducto(Integer id) {
		logger.debug("::::: Mensaje de prueba :::::::");
		Producto Producto = new Producto();	
		List<Producto> listaProductos = new ArrayList<Producto>();
		
		String sql = " SELECT id, descripcion, categoria, precio_unitario, stock_actual, stock_minimo, estado\n" + 
				" FROM api_test.Producto where id='"+id+"'";
				
		try {
			
			RowMapper<Producto> ProductoRow = new ProductoRowMapper();
			listaProductos = getJdbcTemplate().query(sql, ProductoRow);
			
			Producto = listaProductos.get(0);
			
			logger.debug("Se ha traido a la Producto "+listaProductos.get(0).toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return Producto;
	}

	@Override
	public void saveProducto(Producto Producto) {
		
		String sql = "insert into microservicios.Producto (descripcion, categoria, precio_unitario, stock_actual, stock_minimo, estado) "  
				+ "values (?, ?, ?, ?, ?, ?);";
		
		Object[] params = { Producto.getDescripcion(), 
							Producto.getCategoria(), 
							Producto.getPrecio_unitario(), 
							Producto.getStock_actual(), 
							Producto.getStock_minimo(), 
							Producto.getEstado()};
		int[] tipos = { Types.VARCHAR, Types.VARCHAR, Types.DECIMAL, Types.INTEGER, Types.INTEGER, Types.INTEGER};
		
		try {
			
			int filas = getJdbcTemplate().update(sql, params,tipos);
			
			logger.debug("Se han insertado : "+filas+" filas");
			logger.debug("Se ha registrado a la Producto "+Producto.toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

	@Override
	public void deleteProducto(Integer id) {
		int regeliminados = 0;		
		String sql = " delete from Producto where id ='"+id+"'";		
		try {			
			regeliminados = getJdbcTemplate().update(sql);
			logger.debug("Se han eliminado "+regeliminados+" Producto con id = "+id);
		} catch (Exception e) {			
			logger.error(e.getMessage());
		}
	}

}
